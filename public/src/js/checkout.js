
Stripe.setPublishableKey('pk_test_l3crbRAv6t4lHTQvoMDr05FU002pfY2tkb');

$form = $('#checkout-submit');

$form.submit(function (event) {
  $('#charge-error').addClass('hidden');
  $form.find('button').prop('disabled', true);
  Stripe.card.createToken({
    number: $('#card-number').val(),
    cvc: $('#card-cvc').val(),
    exp_month: $('#card-expiry-month').val(),
    exp_year: $('#card-expiry-year').val()
  }, stripeResponseHandler);
  return false;
});

function stripeResponseHandler(status, response) {
  if ($('#payment-1')[0].checked) {
    $form.get(0).submit();
  } else if(response.error) {
    $('#charge-error').removeClass('hidden');
    $('#charge-error').text(response.error.message);
    $form.find('button').prop('disabled', false);
  } else {
    var token = response.id;
    $form.append($('<input type="hidden" name="stripeToken" />').val(token));
    $form.get(0).submit();
  }
} 

