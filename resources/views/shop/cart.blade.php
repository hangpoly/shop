@extends('layouts.master')


@section('title')
  Giỏ Hàng
@endsection

@section('content')
  <div class="row mt-4">
    <div class="col-12">
      <h6 class="m-0">GIỎ HÀNG</h6>
    </div>
  </div>
  @if(Session::has('cart'))
    <div class="row justify-content-center mt-5">
      <div class="col-12 col-md-9 mb-4 p-4 bg-white rounded">
        @foreach ($items as $pro)
          <div class="row">
            <div class="col-md-2 col-6">
              <img class="cart" src="{{ $pro['item']['imagePath'] }}" alt="">
            </div>

            <div class="col-md-6 col-6 h-100">
              <div class="d-flex flex-column align-content-between">
                <div><h6>{{ $pro['item']['title'] }}</h6></div>
               <div><span class="text-primary" style="font-size:0.8rem"><a href="{{ route('product.removeItem', ['id' => $pro['item']['id']] )}}">xóa</a></span></div>
              </div>
            </div>

            <div class="col-md-4 col-12">
              <div class="d-flex justify-content-between flex-xs-column">
                <div>
                  <span class="font-weight-bold">{{ $pro['item']['price'] }} đ</span>
                </div>
                <div class="d-block w-50">
                  <form action="" class="d-block">
                    <div class="input-group input-group-sm">
                      <div class="input-group-prepend">
                        <button type="button" class="btn btn-outline-secondary" onclick="location.href='{{ route('product.reduceFromCart', ['id' => $pro['item']['id'] ]) }}'">-</button>
                      </div>
                      <input type="text" class="form-control d-block w-25" value="{{ $pro['qty'] }}" aria-label="Input group example" aria-describedby="btnGroupAddon">
                      <div class="input-group-append">
                        <button type="button" class="btn btn-outline-secondary" onclick="location.href='{{ route('product.addToCart', ['id' => $pro['item']['id'] ]) }}'">+</button>
                      </div>
                    </div> 
                  </form>
                </div>          
              </div>
            </div>

          </div>
          <hr>
        @endforeach
      </div>
      <div class="col-sm-12 col-md-3">
        <div class="bg-white rounded p-3 pb-3 cl">
          <div class="font-weight-light">
            <span class="">Tạm tính: </span>
            <span class="float-right">{{ number_format($totalPrice) }} đ</span>
          </div>
          <hr>
          <div class="font-weight-light">
            <div class="clearfix">
              <span class="">Thành Tiền: </span>
              <span class="float-right font-weight-bold text-danger" style="font-size: 1.2rem">{{ number_format($totalPrice) }} đ</span>
            </div>
            <div class="clearfix">
              <span class="float-right font-weight-light" style="font-size: 0.8rem;">(đã bao gồm thuế VAT)</span>
            </div>
          </div>
        </div>

        <div class="mt-4">
          <button class="d-block w-100 btn btn-danger" onclick="location.href='{{ route('checkout')}}'">Tiến Hành Đặt Hàng</button>
        </div>
      </div>
    </div>
  @else
    <span>Chưa Có Sản Phẩm</span>
  @endif
@endsection