@extends('layouts.master')


@section('title')
  Sản Phẩm
@endsection

@section('content')
  @if(Session::has('success'))
  <div class="row my-2 justify-content-center">
    <div class="col-sm-6 col-md-4">
      <div id="charge-message" class="alert alert-success">
        {{ Session::get('success')}}
      </div>
    </div>
  </div>
  @endif
  <div class="row mt-3">
    @foreach ($products as $item)
    <div class="col-sm-3 col-md-4 my-5">
      <div class="thumbnail">
        <img src="{{ $item->imagePath }}" class="" alt="...">
        <div class="caption">
          <h5 class="">{{ $item->title }}</h5>
          <p class="description">{{ $item->description }}</p>
          <div class="clear-fix">
            <span class="price">${{ $item->price }}</span>
            <a href="{{ route('product.addToCart', ['id' => $item->id ]) }}" class="btn btn-success float-right">Add to cart</a>
          </div>
        </div>
      </div>
    </div>       
    @endforeach

  </div>
@endsection