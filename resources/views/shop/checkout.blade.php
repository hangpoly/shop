@extends('layouts.master')


@section('title')
  Tiến Hành Đơn Hàng
@endsection

@section('content')
<div class="row mt-4">
  <div class="col-6 col-md-6">
    <h5 class="mb-3">Đặt Hàng</h5>
    {{-- @if ( Session::has('error') ) --}}
    <div id="charge-error" class="alert alert-danger {{ !Session::has('error') ? 'hidden' : ''}}">
      {{ Session::get('error') }}
    </div>
    {{-- @endif --}}
  </div>
</div>
<div class="row mb-5">
  <div class="col-12 col-md-6 py-4 bg-white rounded">
    <div>
      <form action="{{route('checkout')}}" id="checkout-submit" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
          <label class="form-label font-weight-bold" for="#name">Tên</label>
          <input type="text" id="name" name="name" class="form-control">
        </div>

        <div class="form-group">
          <label class="form-label font-weight-bold" for="#phone">Số Điện Thoại</label>
          <input type="text" max="11" id="phone" name="phone" class="form-control">
        </div>

        <div class="form-group">
          <label class="form-label font-weight-bold" for="#address">Địa chỉ giao hàng</label>
          <input type="text" id="address" name="address" class="form-control">
        </div>

        <h6>Hình Thức Thanh Toán</h6>
       
        {{--  --}}
        <div class="custom-control custom-radio">
          <input type="radio" class="custom-control-input" value="1" id="payment-1" name="payment-method" required>
          <label class="custom-control-label" for="payment-1">Trả Tiền Mặt Khi Nhận Hàng</label>
        </div>
        <div class="custom-control custom-radio mb-3">
          <input type="radio" class="custom-control-input" value="2" id="payment-2" name="payment-method" checked required>
          <label class="custom-control-label" for="payment-2">Thẻ Thanh Toán</label>
        </div>

        {{-- Visa Payment --}}
        <div  id="card-payment">
          <div class="form-group">
            <label class="form-label font-weight-bold" for="#card-name">Tên Chủ Thẻ</label>
            <input type="text" id="card-name" name="card-name" class="form-control">
          </div>
    
          <div class="form-group">
            <label class="form-label font-weight-bold" for="#card-number">Số Thẻ</label>
            <input type="text" id="card-number" name="card-number" class="form-control">
          </div>
    
          <h6>Hết Hạn</h6>
          <div class="form-row">
            <div class="form-group form-inline col-6 col-md-3">
              <label for="#card-expiry-month">Tháng</label>
              <input type="number" min="1" max="12" id="card-expiry-month" name="card-expiry-month" class="form-control">
            </div>
            <div class="form-group col-6 col-md-3">
              <label for="#card-expiry-year">Năm</label>
              <input type="number" min="2019" max="9999"id="card-expiry-year" name="card-expiry-year" class="form-control">
            </div>
          </div>
    
          <div class="form-group">
            <label for="#card-cvc">CVC</label>
            <input type="number" id="card-cvc" name="card-cvc" class="form-control">
          </div>
        </div>

          {{-- submit-  --}}
        <div class="mb-2">
          <button class="btn btn-success">Đặt Hàng</button>
        </div>
        <div>
          <h6 class="mb-2">Tổng hóa đơn: <span class="text-danger">{{ number_format($total) }} đ</span></h6>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $("#payment-1").click(function(){
    if (this.checked) {
      $('#card-payment').hide();
    } else {
      $('#card-payment').show();
    }
  });
  $("#payment-2").click(function(){
    if (this.checked) {
      $('#card-payment').show();
    } else {
      $('#card-payment').hide();
    }
  });
</script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="{{ URL::to('src/js/checkout.js') }}"></script>
@endsection