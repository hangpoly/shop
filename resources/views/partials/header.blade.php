<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">ALADIN</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item mr-3 active">
        <a class="nav-link" href="{{ route('product.index') }}">Giày<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item mr-3">
        <a class="nav-link" href="#">Quần Áo</a>
      </li>
      <li class="nav-item mr-3">
        <form class="form-inline my-2 my-lg-0">
          <div class="input-group">
            <input class="form-control" type="search" placeholder="Quần áo thể thao..." aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-light border-none my-2 my-sm-0" type="submit">Tìm Kiếm</button>
            </div>
          </div>
        </form>
      </li>
    </ul>
   
    <div class="my-2 my-lg-0">
      <ul class="navbar navbar-nav mr-2">
        <li class="ml-3 nav-item">
          <a href="{{route('product.shoppingCart') }}" class="nav-link text-light">
            @if(Session::has('cart'))
              <span class="badge badge-pill badge-danger">{{ Session::get('cart')->totalQty }}</span>
            @endif
            <i class="fas fa-shopping-cart"></i>
            <span> Giỏ Hàng</span>
          </a>
        </li>
        <li class="ml-3 nav-item dropdown">
          <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-user"></i> Người dùng
          </a>
          <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            @if(Auth::check())
              <li class="dropdown-item"><a class="nav-link text-dark" href="{{ route('user.profile') }}">Cá Nhân</a></li>
              <div class="dropdown-divider"></div>
              <li class="dropdown-item">
                <a class="nav-link text-dark" href="{{ route('user.logout') }}"><i class="fas fa-sign-out-alt"></i> Thoát</a>
              </li>
            @else
              <li class="dropdown-item"><a class="nav-link text-dark" href="{{ route('user.signup') }}">Đăng Ký</a></li>
              <li class="dropdown-item"><a class="nav-link text-dark" href="{{ route('user.signin') }}">Đăng Nhập</a></li>
            @endif
          </ul>
        </li>
      </ul>
    </div>
    
  </div>
</nav>