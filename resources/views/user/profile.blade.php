@extends('layouts.master')

@section('title')
  Hồ sơ
@endsection

@section('content')
<div class="row mt-4">
</div>
<div class="row justify-content-center">
  {{-- <div class="col-3 col-md-3">
    <div class="bg-white">
      <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action active">Thông Tin Mua Hàng</a>
        <a href=" {{ route('user.signin')}} " class="list-group-item list-group-item-action">Lịch Sử Mua Hàng</a>
      </div>
    </div>
  </div> --}}

  <div class="col-9 col-md-9">
    <h3>Lịch sử mua hàng của tôi</h3>
    <div>
    @foreach($orders as $order)
      <div class="card mb-5">
        @foreach ($order->cart->items as $item)
        <div class="card-body">
          <div class="d-flex justify-content-between">
            <div>
              <img src="{{$item['item']['imagePath']}}" width="70px" heigh="70px" alt="">
            </div>
            <div>{{ $item['item']['title'] }} X {{ $item['qty'] }} </div>
            <div>{{ number_format($item['price']) }} đ</div>
          </div>
        </div>    
        @endforeach
        <div class="card-footer">
          <span class="font-weight-bold">Tổng Tiền:</span> {{ number_format($order->cart->totalPrice) }} đ
        </div>
      </div>
    @endforeach
    </div>
  </div>
</div>
@endsection