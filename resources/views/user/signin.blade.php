@extends('layouts.master')

@section('title')
  Đăng Nhập
@endsection

@section('content')
<div class="mt-5"></div>
<div class="row justify-content-md-center">
  <div class="col-md-4 col-12">
    <h1>Đăng Nhập</h1>
    @if(count($errors) > 0)
      <div class="alert alert-danger">
        @foreach ($errors->all() as  $err)
        <p>{{ $err}}</p>
        @endforeach
      </div>
    @endif

    <form method="POST" action="{{ route('user.signin') }}">
      {{ csrf_field() }}
      <div class="form-group">
        <label class="form-label" for="email">Email</label>
        <input class="form-control" id="email" type="email" name="email" >
      </div>
      <div class="form-group">
        <label class="form-label" for="password">Mật khẩu</label>
        <input class="form-control" id="password" type="password" name="password">
      </div>
      <div class="clear-fix">
        <button class="btn btn-success" type="submit">Đăng Nhập</button>
        {{-- <button class="btn btn-outline-primary float-right" type="button" onclick="location.href='{{ route('user.signup') }}'">Đăng ký</button> --}}
      </div>
      <div class="mt-2">
        <a class="text-decoration-underlined font-weight-light" href=" {{route('user.signup')}}">Đăng ký tài khoản</a>
      </div>
    </form>
  </div>
</div>
@endsection