<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function getSignup () {
        return view('user.signup');
    }

    public function postSignup(Request $req) {
        $this->validate($req, [
            'email' => 'email|required|unique:users',
            'password' => 'required|min:3',
        ]);
        $email = $req->input('email');
        $password = $req->input('password');
        $user = new User([
            'email' => $email,
            'password' => bcrypt($password),
            'role_id' => 3  
        ]); 

        $user->save();
        Auth::login($user);

        return redirect()->route('product.index');
        // return view('user.signup', compact('email', 'password'));
    }

    public function getSignin() {
        return view('user.signin');
    }

    public function postSignin(Request $req) {
        $this->validate($req, [
            'email' => 'email|required',
            'password' => 'required|min:3'
        ]);
        
        $email = $req->input('email');
        $password = $req->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            if(Session::has('oldUrl')) {
                $oldUrl = Session::get('oldUrl');
                return redirect()->to($oldUrl);
            }
            return redirect()->route('user.profile');
        }

        return redirect()->back();
    }

    public function getProfile(){
        $orders = Auth::user()->orders;

        $orders->transform(function($order, $key){
            $order->cart = unserialize($order->cart);
            return $order;
        });  
        return view('user.profile', ['orders' => $orders]);
    }

    public function getLogout() {
        Auth::logout();
        return redirect()->route('user.signin');
    }
    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function create()
    // {
    //     //
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     //
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    //     return view('user.profile');
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit($id)
    // {
    //     //
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     //
    // }
}
