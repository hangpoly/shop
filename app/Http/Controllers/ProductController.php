<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Cart;
use App\Order;
use Stripe\Stripe;
use Stripe\Charge;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('shop.index', [
            'products' => $products
        ]);
    }

    public function addToCart(Request $req, $id) {
        $product = Product::findOrFail($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $id);

        $req->session()->put('cart', $cart);
        // dd($req->session()->get('cart'));
        return redirect()->back();
    }

    public function reduceFromCart($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->desc($id);
        Session::put('cart', $cart);
        if ($cart->totalQty <=0 ) {
            Session::forget('cart', $cart);
        }
        return redirect()->back();
    }

    public function removeItem($id){
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        Session::put('cart', $cart);
        if ($cart->totalQty <=0 ) {
            Session::forget('cart', $cart);
        }
        return redirect()->back();
    }

    public function getCart() {
        if (!Session::has('cart')) {
            return view('shop.cart', ['products' => null]);
        } else {
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            return view('shop.cart', ['items' => $cart->items, 'totalPrice' => $cart->totalPrice]);     
        }
    }


    public function getCheckout() {
        if (!Session::has('cart')) {
            return view('shop.cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice; 
        return view('shop.checkout', ['total' => $total]);
    }

    public function postCheckout(Request $req) {
        if (!Session::has('cart')) {
            return redirect()->route('product.shoppingCart');
        } 
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $tok_visa = $req->input('stripeToken');
        Stripe::setApiKey('sk_test_itoOP6VRXM6FyAJSGu4z1Ri0009OG6RjHd');

        $paymentMethod = $req->input('payment-method');
        if ($paymentMethod == 1) {
            $order = new Order();
            $order->cart = serialize($cart);
            $order->user_address = $req->input('address');
            $order->name =$req->input('name');
            $order->phone =$req->input('phone');
            $order->payment_method_id =$req->input('payment-method');
            $order->payment_id = 'none-method';
            Auth::user()->orders()->save($order);
        } else {
            try {
                $charge = Charge::create([
                    "amount" => $cart->totalPrice,
                    "currency" => "vnd",
                    "source" => $tok_visa, // obtained with Stripe.js
                    "description" => "Thanh toán qua thẻ"
                ]);
                $order = new Order();
                $order->cart = serialize($cart);
                $order->user_address = $req->input('address');
                $order->name =$req->input('name');
                $order->phone =$req->input('phone');
                $order->payment_method_id =$req->input('payment-method');
                $order->payment_id = $charge->id;
                Auth::user()->orders()->save($order);
            } catch (\Exception $e){
                return redirect()->route('checkout')->with('error', $e->getMessage());
            }
        }

        Session::forget('cart');
        return redirect()->route('product.index')->with('success', 'Thanh Toán Thành Công');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
