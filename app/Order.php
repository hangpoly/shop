<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'name', 'address', 'phone', 'payment-method', 'cart'
    ];

    //
    public function user(){
        return $this->belongsTo('App\User');
    }
}
