<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductTalbeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $product = new Product([
            'imagePath' => 'https://giaygiare.vn/upload/sanpham/large/adidas-prophere-trang-nam-nu.jpg',
            'title' => 'Prophere',
            'description' => 'super cool sneaker',
            'price' => 1250500
        ]);

        $product->save();

        $product1 = new Product([
            'imagePath' => 'https://giaygiare.vn/upload/sanpham/large/adidas-prophere-trang-nam-nu.jpg',
            'title' => 'Prophere',
            'description' => 'super cool sneaker',
            'price' => 1850000
        ]);

        $product1->save();

        $product2 = new Product([
            'imagePath' => 'https://giaygiare.vn/upload/sanpham/large/adidas-prophere-xam-cam-nam-nu.jpg',
            'title' => 'Prophere',
            'description' => 'super cool sneaker',
            'price' => 1750000
        ]);

        $product2->save();

        $product3 = new Product([
            'imagePath' => 'https://giaygiare.vn/upload/sanpham/large/adidas-prophere-xanh-ngoc-nu.jpg',
            'title' => 'Prophere',
            'description' => 'super cool sneaker',
            'price' => 1250000
        ]);

        $product3->save();
    }
}
